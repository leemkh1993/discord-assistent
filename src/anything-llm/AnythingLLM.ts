import { isString, Inquirer } from "@/utils";

enum AnythingLLMEndpoint {
  ROOT = "api/v1",
  WORKSPACES = "workspaces",
  WORKSPACE = "workspace",
  STREAM_CHAT = "stream-chat",
}

type Workspace = {
  id: number;
  name: string;
  slug: string;
};

type WorkspaceAPIResponse = {
  workspaces: Workspace[];
};

export default class AnythingLLM extends Inquirer {
  private defaultHeader: HeadersInit;
  private workspace: Workspace | null = null;

  constructor(
    private host: string,
    private apiKey: string,
  ) {
    if (!isString(host) || !isString(apiKey)) {
      throw new Error("AnythingLLM: Host and API key cannot be empty");
    }

    super();
    this.host = host;
    this.apiKey = apiKey;
    this.defaultHeader = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${this.apiKey}`,
    };
  }

  private getEndpoint(
    isWorkspaces: boolean,
    slug?: string,
    endpoint?: AnythingLLMEndpoint,
  ): string {
    return `${this.host}/${AnythingLLMEndpoint.ROOT}/${isWorkspaces ? AnythingLLMEndpoint.WORKSPACES : AnythingLLMEndpoint.WORKSPACE}${slug ? `/${slug}` : ""}${endpoint ? `/${endpoint}` : ""}`;
  }

  async setWorkspace(): Promise<void> {
    const req = await fetch(this.getEndpoint(true), {
      method: "GET",
      headers: this.defaultHeader,
    });

    if (!req.ok) {
      console.log(req.status, req.statusText);
      throw new Error("Failed to fetch workspace ID");
    }

    const res = (await req.json()) as WorkspaceAPIResponse;

    if (!res.workspaces.length) {
      console.log("No workspaces found");
      return;
    }

    const workspace = await this.singleSelect<Workspace>(
      res.workspaces,
      (workspace) => {
        return `ID: ${workspace.id}, Workspace Name: ${workspace.name}, slug: ${workspace.slug}`;
      },
      "Select a workspace to use",
    );

    this.workspace = workspace;
  }

  async streamChat(message: string): Promise<string> {
    if (!this.workspace) {
      throw new Error("No workspace selected");
    }

    console.log("Waiting response from LLM...");

    const req = await fetch(
      this.getEndpoint(
        false,
        this.workspace.slug,
        AnythingLLMEndpoint.STREAM_CHAT,
      ),
      {
        method: "POST",
        headers: {
          ...this.defaultHeader,
          accept: "text/event-stream",
        },
        body: JSON.stringify({
          mode: "chat",
          message: message,
        }),
      },
    );

    if (!req.ok) {
      console.error(req.status, req.statusText);
      throw new Error("Failed to fetch stream chat");
    }

    const reader = req.body!!.getReader();

    let llmRes = "";

    while (true) {
      const { done, value } = await reader.read();
      if (done) {
        break;
      }

      const buffer = Buffer.from(value);
      const data: { textResponse: string } = JSON.parse(
        buffer.toString("utf-8").replace("data: ", ""),
      );
      llmRes = llmRes + data.textResponse;
    }

    return llmRes;
  }

  taskBreakdownPrompt = (task: string): string => {
    return `You are my personal assistant. Your job is to break down a complicated task to different sub tasks in point form.

    Here is one of the example:
    Tasks: Steps to setup router
    Response:
    <#STEP#>Step 1: Decide where to place the router<#STEP#>
      <#DESC#>Review the current state of Subanana’s Google Analytics (GA) implementation<#DESC#>
      <#DESC#>Identify the new events that need to be implemented<#DESC#>
    <#STEP#>Step 2: Connect to the Internet<#STEP#>
      <#DESC#>Review the current state of Subanana’s Google Analytics (GA) implementation<#DESC#>
      <#DESC#>Identify the new events that need to be implemented<#DESC#>
    <#STEP#>Step 3: Configure the wireless router gateway<#STEP#>
      <#DESC#>Review the current state of Subanana’s Google Analytics (GA) implementation<#DESC#>
      <#DESC#>Identify the new events that need to be implemented<#DESC#>
    <#STEP#>Step 4: Connect gateway to router<#STEP#>
      <#DESC#>Review the current state of Subanana’s Google Analytics (GA) implementation<#DESC#>
      <#DESC#>Identify the new events that need to be implemented<#DESC#>
    <#STEP#>Step 5: Use app or web dashboard<#STEP#>
      <#DESC#>Review the current state of Subanana’s Google Analytics (GA) implementation<#DESC#>
      <#DESC#>Identify the new events that need to be implemented<#DESC#>

    Task: ${task}
    `;
  };
}
