import inquirer from "inquirer";

export class Inquirer {
  constructor() {}

  async singleSelect<T>(
    options: T[],
    format: (obj: T) => string,
    message: string,
  ): Promise<T> {
    const { selection } = await inquirer.prompt([
      {
        type: "list",
        name: "selection",
        message: message,
        choices: options.map(format),
        filter: (val: string) => {
          return options.find((option) => format(option) === val);
        },
      },
    ]);

    return selection;
  }

  async askForInput(message: string): Promise<string> {
    const { input } = await inquirer.prompt([
      {
        type: "input",
        name: "input",
        message: message,
      },
    ]);

    return input;
  }

  async confirm(message: string): Promise<boolean> {
    const { confirm } = await inquirer.prompt([
      {
        type: "confirm",
        name: "confirm",
        message: message,
      },
    ]);

    return confirm;
  }
}
