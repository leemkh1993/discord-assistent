export default {
  extends: ["prettier"],
  plugins: ["@typescript-eslint", "unused-imports"],
  rules: {
    "sort-imports": [
      "warn",
      {
        ignoreDeclarationSort: true,
      },
    ],
    "import/order": [
      "warn",
      {
        "newlines-between": "never",
        alphabetize: {
          order: "asc",
          orderImportKind: "asc",
        },
      },
    ],
    "no-unused-vars": "off",
    "@typescript-eslint/no-unused-vars": "off",
    "unused-imports/no-unused-imports": "warn",
    "unused-imports/no-unused-vars": "warn",
  },
};
