import {
  ANYTHING_LLM_EP,
  ANYTHING_LLM_API_KEY,
  NOTION_API_KEY,
  isString,
  postProcess,
  printTasks,
} from "@/utils";
import AnythingLLM from "@/anything-llm/AnythingLLM";
import Notion from "@/notion/Notion";

enum Command {
  CHAT = "chat",
  TASK_BREAKDOWN = "task-breakdown",
  DONE = "done",
}

const main = async () => {
  const llm = new AnythingLLM(ANYTHING_LLM_EP, ANYTHING_LLM_API_KEY!!);
  const notion = new Notion(NOTION_API_KEY!!);

  await llm.setWorkspace();
  await notion.setDatabases();

  while (true) {
    const command = await llm.singleSelect<string>(
      [Command.CHAT, Command.TASK_BREAKDOWN, Command.DONE],
      (command) => command,
      "What do you want to do?",
    );

    const message = await llm.askForInput(
      "What do you want the LLM to do for you?",
    );

    if (!isString(message)) {
      console.log("Need a message, try again");
      continue;
    }

    let response: string;

    switch (command) {
      case Command.DONE:
        console.log("Goodbye!");
        return;
      case Command.CHAT:
        response = await llm.streamChat(message);
        console.log(`LLM said:\n\n${response}`);
        break;
      case Command.TASK_BREAKDOWN:
        response = await llm.streamChat(llm.taskBreakdownPrompt(message));

        const tasks = postProcess(response);

        printTasks(tasks);

        const confirm = await llm.confirm(
          "Do you want to break the tasks down further? If not, the tasks will be written to Notion.",
        );

        // Create main task
        const mainTaskId = await notion.createMainTask({
          title: message,
        });

        for (const task of tasks) {
          // Create sub-tasks and add relation to main task
          if (!confirm) {
            console.log("Writing to Notion...");
            await notion.createSubtask(task, mainTaskId);
            continue;
          }

          // TODO: Implement sub-task breakdown
          for (const subTask of task.description!!) {
            console.log(subTask);
          }
        }
        break;
      default:
        console.log("Invalid command");
        break;
    }
  }
};

main();
